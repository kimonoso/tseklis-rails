class ApplicationMailer < ActionMailer::Base
  default from: 'admin@tseklis.com'
  layout 'mailer'
end
